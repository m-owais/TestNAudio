﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace TestNAudio
{    
    public static class Logger
    {
        private static readonly log4net.ILog _log = GetLogger(typeof(Logger));

        static ILog GetLogger(Type type)
        {
            return LogManager.GetLogger(type);
        }
        
        public static void Info(string message, params object[] arg)
        {
            _log.InfoFormat(message, arg);
        }

        public static void Debug(string message, params object[] arg)
        {
            _log.DebugFormat(message, arg);
        }

        public static void Warn(string message, params object[] arg)
        {
            _log.WarnFormat(message, arg);
        }

        public static void Error(string message, params object[] arg)
        {
            _log.ErrorFormat(message, arg);
        }

        public static void Init()
        {
            // Load configuration and start log4net
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
        }
    }
}
