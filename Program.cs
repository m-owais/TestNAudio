﻿using Google.Api.Gax.Grpc;
using Google.Cloud.Speech.V1;
using NAudio.Wave;
using NAudio.Wave.Asio;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using static Google.Cloud.Speech.V1.SpeechClient;

namespace TestNAudio
{
    public enum EnStateChannel
    {   
        WaitingActivity,
        Buffering,        
    };

    class Program
    {
        const string Version = "Beta-8";
        /// <summary>
        /// Tests google speech to text API with streaming mode
        /// Integrated on 28-MAY-2021
        /// Owais
        /// Amended to use unmanaged pointers instead of managed float list and arrays
        /// Added more app.config options and new params in AudioChannelSettings
        /// 
        /// June 20-27
        /// Changed logic to use buffering when doing actual transcription. This way no frames are missed
        /// 
        /// 3-JUL-2021
        /// Added non constant frame buffer size handling, it fixes the delay counting to determing silence
        /// (SampleRate / samplesPerBuffer) * GetIdleTimeSeconds(); e.g.
        /// 48000 / 512 * 3 = 281 idle consecutive frame count means that channel is now idle
        /// </summary>
        static int[] m_cntIdleFrames;
        static int[] m_cntSentFrames;
        volatile static WaveFileWriter[] m_arrAsioWaveFileWriter;
        static AsioOut m_Asio;
        volatile static IntPtr[] m_prePendSamplesByChNumber;
        volatile static ConcurrentQueue<IntPtr>[] m_CQSamplesByChNumber;
        volatile static byte[][] m_soundBytesByChNumber;
        static SpeechClient m_speechClient;
        volatile static StreamingRecognizeStream[] m_srStream;
        static RttDbHelper m_RttDbHelper;
        static bool m_IsDBAvailable, mIsAsioWorking = true, mIsDbDeleteDue = false;
        static Timer m_TimerDBSave, m_TimerAsioWatchDog, m_TimerDBDeletePeriodic;
        static AsioSampleType m_SampleType;
        static int m_SamplePerBuffer;
        static ConcurrentQueue<TextMessage> m_QueueRTT = new ConcurrentQueue<TextMessage>();
        static RecognitionConfig m_RecCfg;
        volatile static Task[] m_ChannelTranTask;
        static EnStateChannel[] m_StateChannel;
        volatile static int m_cntMaxFramesPerSession;

        [STAThread]
        static void Main(string[] args)
        {
            //log4net wrapper func holder init
            Logger.Init();

            SpeechClientBuilder scb = new SpeechClientBuilder();
            scb.CredentialsPath = System.IO.Directory.GetCurrentDirectory() + "\\Credentials.json";
            m_speechClient = scb.Build();

            if(m_speechClient == null)
            {
                Logger.Error("Could not create Google Speech service client. Aborting.");
                Environment.Exit(0);
            }

            Logger.Debug("RTT Software Version: " + Version);
            string[] strDrivers = AsioOut.GetDriverNames();
            Logger.Info("Installed ASIO drivers List: ");
            for(int i=0; i < strDrivers.Length; i++)
                Logger.Info("Driver {0} = {1}", i, strDrivers[i]);

            int indexAsioDriver = SettingsHelper.GetAsioDriverIndex(strDrivers);
            if (indexAsioDriver == -1)
                Environment.Exit(-1);

            //Use selected ASIO driver
            m_Asio = new AsioOut(indexAsioDriver); 
            Logger.Info("Input channels count: " + m_Asio.DriverInputChannelCount);            
            m_CQSamplesByChNumber = new ConcurrentQueue<IntPtr>[m_Asio.DriverInputChannelCount];
            m_prePendSamplesByChNumber = new IntPtr[m_Asio.DriverInputChannelCount];
            m_cntIdleFrames = new int[m_Asio.DriverInputChannelCount];
            m_cntSentFrames = new int[m_Asio.DriverInputChannelCount];
            m_soundBytesByChNumber = new byte[m_Asio.DriverInputChannelCount][];
            m_ChannelTranTask = new Task[m_Asio.DriverInputChannelCount];
            m_RttDbHelper = new RttDbHelper();
            m_RttDbHelper.Open();

            m_IsDBAvailable = m_RttDbHelper.IsDBAvailable();
            if (m_IsDBAvailable)
            {
                Logger.Info("RTT transcription database is available for logging.");
                m_RttDbHelper.DeleteMessagesOlderThanHours(SettingsHelper.GetDbHoursDelete());
            }
            else
                Logger.Warn("RTT transcription database is NOT available for logging. Check connection settings or database status.");

            bool bChCfgReadResult = AudioChannelConfig.ReadSettingsFromJson();
            if (bChCfgReadResult)
                Logger.Info("Audio Channel configuration read successfully from JSON file.");
            else
                Logger.Warn("Failed to read Audio Channel configuration from JSON file.");            

            m_arrAsioWaveFileWriter = new WaveFileWriter[m_Asio.DriverInputChannelCount];
            m_srStream = new StreamingRecognizeStream[m_Asio.DriverInputChannelCount];

            for (int i = 0; i < m_Asio.DriverInputChannelCount; i++)
                Logger.Info("Input device {0}, Channel name: {1}", i, m_Asio.AsioInputChannelName(i));

            m_RecCfg = SettingsHelper.GetRecognitionCfg();
            m_TimerDBSave = new Timer(TimerSaveDB, null, 500, 500);
            m_TimerAsioWatchDog = new Timer(TimerWatchDog, null, 3000, 3000);//Run every 3 seconds to see if ASIO working
            m_TimerDBDeletePeriodic = new Timer(TimerDBDelete, null, 60 * 60 * 1000, 60 * 60 * 1000);

            if (!m_Asio.IsSampleRateSupported(SettingsHelper.SampleRate))
            {
                Logger.Error("Configured sample rate \"{0}\" is not supported. Aborting.", SettingsHelper.SampleRate);
                Environment.Exit(-2);
            }           

            for (int i = 0; i < m_ChannelTranTask.Length; i++)
            {
                int transIndex = i;                
                m_CQSamplesByChNumber[i] = new ConcurrentQueue<IntPtr>();
                m_ChannelTranTask[i] = Task.Factory.StartNew(() => Task_StreamAPI(transIndex), TaskCreationOptions.LongRunning);
            }

            m_StateChannel = new EnStateChannel[m_Asio.DriverInputChannelCount];

            m_Asio.InputChannelOffset = 0; 
            m_Asio.InitRecordAndPlayback(null, m_Asio.DriverInputChannelCount, SettingsHelper.SampleRate);
            m_SamplePerBuffer = m_Asio.FramesPerBuffer;
            m_cntMaxFramesPerSession = SettingsHelper.GetMaxAllowedFrameCount(m_SamplePerBuffer);
            Logger.Info("Frames per buffer: " + m_Asio.FramesPerBuffer);
            Logger.Info("Sample Rate: " + SettingsHelper.GetSampleRate());
            Logger.Info("Max duration of transcribed audio (in secs): " + SettingsHelper.GetMaximumTranscribeSeconds());
            Logger.Info("Max Frame count in a stream session: " + m_cntMaxFramesPerSession);
            m_Asio.AudioAvailable += OnAsioOutAudioAvailable;
            Logger.Info("Ready for recording on all channels");
            m_Asio.Play(); // start recording

            try
            {
                while (true)
                {
                    try
                    {
                        if (!mIsAsioWorking)
                        {
                            m_TimerAsioWatchDog.Change(Timeout.Infinite, Timeout.Infinite);
                            Logger.Warn("Restarting ASIO recording after failure detected.");

                            if (m_Asio != null)
                            {
                                m_Asio.Stop();
                                m_Asio.AudioAvailable -= OnAsioOutAudioAvailable;
                            }

                            m_Asio = null;

                            indexAsioDriver = SettingsHelper.GetAsioDriverIndex(AsioOut.GetDriverNames());
                            if (indexAsioDriver == -1)
                            {
                                Logger.Warn("No ASIO drivers found when restarting recording. Exiting program.");
                                Environment.Exit(-1);
                            }
                            
                            //Use selected ASIO driver
                            m_Asio = new AsioOut(indexAsioDriver);
                            m_Asio.AudioAvailable += OnAsioOutAudioAvailable;
                            m_Asio.InitRecordAndPlayback(null, m_Asio.DriverInputChannelCount, SettingsHelper.SampleRate);
                            m_Asio.Play(); // start recording
                            mIsAsioWorking = true;
                            m_TimerAsioWatchDog.Change(3000, 3000);
                            Logger.Debug("ASIO recording RE-STARTED SUCCESSFULLY.");
                        }

                        Thread.Sleep(100);
                    }
                    catch (Exception exc)
                    {
                        Logger.Error("Error restarting ASIO recording: " + exc.ToString());
                    }
                }
            }
            catch (Exception exc)
            {
                Logger.Error("Error occured main loop: " + exc.ToString());
            }

            Logger.Error("End of program");
        }        

        public static async Task Task_StreamAPI(int chIndex)
        {
            int sizeAlloc;
            IntPtr iptr;

            while (true)
            {
                try
                {
                    bool bDQ = m_CQSamplesByChNumber[chIndex].TryDequeue(out iptr) == false;

                    if (bDQ)
                    {
                        await Task.Delay(5);
                        continue;
                    }

                    bool isIdle = IsIdle(iptr, m_SamplePerBuffer, m_SampleType);

                    switch (m_StateChannel[chIndex])
                    {
                        case EnStateChannel.WaitingActivity:
                            {
                                if (isIdle)
                                {
                                    AddToPrependBuffer(chIndex, iptr);                                    
                                    m_cntIdleFrames[chIndex] = 0;
                                    m_cntSentFrames[chIndex] = 0;                                    
                                }
                                else
                                {
                                    Logger.Info("Transcribe stream starting for channel " + chIndex + ", Task ID=" + Task.CurrentId);
                                    if (TranscribeInitial(chIndex, iptr))
                                    {
                                        m_StateChannel[chIndex] = EnStateChannel.Buffering;
                                        m_cntSentFrames[chIndex]++;
                                    }
                                }
                            }
                            break;

                        case EnStateChannel.Buffering:
                            {
                                //Check for overflow case, exceeding the transcription limitation of google's service
                                if (m_cntSentFrames[chIndex] >= m_cntMaxFramesPerSession)
                                {
                                    Logger.Info("Length in seconds exceeded and forcing transcription, for channel " + chIndex);
                                    TranscribeLast(chIndex, iptr);
                                    Logger.Info("Finished Transcription streaming for channel " + chIndex + ", Waiting for new audio data");
                                    m_StateChannel[chIndex] = EnStateChannel.WaitingActivity;
                                    m_cntIdleFrames[chIndex] = 0;
                                    m_cntSentFrames[chIndex] = 0;
                                    AddToPrependBuffer(chIndex, iptr); //we do this to make the next transcribe start a little smoother
                                    break; //we need this break so it doesnot trigger further 'TranscribeLast' or 'TranscribeMiddle' below if else block
                                }

                                if (isIdle)
                                {
                                    m_cntIdleFrames[chIndex]++;

                                    if (m_cntIdleFrames[chIndex] > SettingsHelper.GetMaxIdleFrameCount(m_SamplePerBuffer))
                                    {
                                        Logger.Info("Idle detected for channel " + chIndex);
                                        TranscribeLast(chIndex, iptr);
                                        Logger.Info("Finished Transcription streaming for channel " + chIndex + ", Waiting for new audio data");
                                        m_StateChannel[chIndex] = EnStateChannel.WaitingActivity;
                                        m_cntIdleFrames[chIndex] = 0;
                                        m_cntSentFrames[chIndex] = 0;
                                    }
                                    else
                                    {
                                        if (TranscribeMiddle(chIndex, iptr))
                                            m_cntSentFrames[chIndex]++;
                                    }
                                }
                                else
                                {
                                    if (TranscribeMiddle(chIndex, iptr))
                                        m_cntSentFrames[chIndex]++;

                                    m_cntIdleFrames[chIndex] = 0;
                                }                                
                            }
                            break;

                        default:
                            break;
                    }

                    //release memory of frame read from CQ of channel
                    try
                    {
                        if (iptr != IntPtr.Zero)
                        {
                            Marshal.FreeHGlobal(iptr);
                            iptr = IntPtr.Zero;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("Failed to free memory for channel: " + chIndex + ", " + ex.ToString());
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("Exception in Task_StreamAPI: " + ex.ToString());
                }
            }

            Logger.Warn("END of Task for " + chIndex);
        }

        public static void TimerSaveDB(Object stateInfo)
        {
            int CntAdded = 0;
            TextMessage tmout = null;

            m_TimerDBSave.Change(Timeout.Infinite, Timeout.Infinite);

            try
            {
                while (m_QueueRTT.TryDequeue(out tmout))
                {
                    if (m_RttDbHelper.GetContext() != null)
                    {
                        m_RttDbHelper.GetContext().TextMessages.Add(tmout);
                        CntAdded++;
                    }
                }

                if (CntAdded > 0)
                {
                    m_RttDbHelper.GetContext().SaveChanges();
                    Logger.Info("RTT records persisted to database: " + CntAdded);
                }
            }
            catch (Exception exc)
            {
                Logger.Error("Error saving records to database: " + exc.ToString());
            }                      

            try
            {
                if (mIsDbDeleteDue)
                {
                    Logger.Warn("Purging old database records...");
                    m_RttDbHelper.DeleteMessagesOlderThanHours(SettingsHelper.GetDbHoursDelete());
                    mIsDbDeleteDue = false;
                }
            }
            catch (Exception exc)
            {
                Logger.Error("Error restarting ASIO recording: " + exc.ToString());//
            }

            m_TimerDBSave.Change(500, 500);
        }

        private static void TimerWatchDog(object state)
        {
            Logger.Error("ASIO Watchdog timer triggered. Flagging failure state for restarting.");
            mIsAsioWorking = false;
        }

        public static void TimerDBDelete(object state)
        {
            Logger.Debug("Hourly DB cleanup flagged.");
            mIsDbDeleteDue = true;
        }

        static void OnAsioOutAudioAvailable(object sender, AsioAudioAvailableEventArgs e)
        {
            try
            {
                m_TimerAsioWatchDog.Change(3000, 3000);
                m_SampleType = e.AsioSampleType;

                //get samples for each channel
                for (int chIndex = 0; chIndex < m_Asio.DriverInputChannelCount; chIndex++)
                {
                    //skip disabled channels
                    if (!AudioChannelConfig.GetConfig(chIndex).Enabled)
                        continue;

                    IntPtr iptr = e.InputBuffers[chIndex];
                    AddSampleCopyToCQ(chIndex, iptr);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Exception in OnAsioOutAudioAvailable: " + ex.ToString());
            }
        }

        private static void AddSampleCopyToCQ(int chIndex, IntPtr iptr)
        {
            int sizeAlloc;

            unsafe
            {
                IntPtr iPtrCopy = SettingsHelper.AllocateMemory(m_SamplePerBuffer, m_SampleType, out sizeAlloc);
                Buffer.MemoryCopy((void*)iptr, (void*)iPtrCopy, sizeAlloc, sizeAlloc);
                m_CQSamplesByChNumber[chIndex].Enqueue(iPtrCopy);
            }
        }

        static bool TranscribeInitial(int chIndex, IntPtr iptrFirst)
        {
            bool result = true;
            IntPtr iptrPrepend;
            bool bSaveEnabledToFile = AudioChannelConfig.GetConfig(chIndex).Enabled && AudioChannelConfig.GetConfig(chIndex).SaveWavFile;
            bool bPossibleToWriteToSaveFile;

            try
            {
                if (bSaveEnabledToFile)
                {
                    Logger.Debug("Starting audio save in wav file for channel: " + chIndex);
                    m_arrAsioWaveFileWriter[chIndex] = new WaveFileWriter(AudioChannelConfig.GetConfig(chIndex).GetSavePath(), new WaveFormat(SettingsHelper.SampleRate, 1));
                }

                m_srStream[chIndex] = m_speechClient.StreamingRecognize();
                StreamingRecognizeRequest request = new StreamingRecognizeRequest();
                request.StreamingConfig = new StreamingRecognitionConfig();
                request.StreamingConfig.Config = m_RecCfg;

                request.StreamingConfig.InterimResults = false;
                m_srStream[chIndex].WriteAsync(request).Wait();
                iptrPrepend = m_prePendSamplesByChNumber[chIndex];

                List<IntPtr> lptrs = new List<IntPtr>();
                if (iptrPrepend != IntPtr.Zero) lptrs.Add(iptrPrepend);
                if (iptrFirst != IntPtr.Zero) lptrs.Add(iptrFirst);

                bPossibleToWriteToSaveFile = bSaveEnabledToFile && m_arrAsioWaveFileWriter[chIndex] != null && m_arrAsioWaveFileWriter[chIndex].CanWrite;
                WriteSamplesToByteArray(chIndex, lptrs, m_SampleType, m_SamplePerBuffer, bPossibleToWriteToSaveFile);

                m_srStream[chIndex].WriteAsync(
                     new StreamingRecognizeRequest()
                     {
                         AudioContent = Google.Protobuf.ByteString.CopyFrom(m_soundBytesByChNumber[chIndex])
                     }).Wait();                
            }
            catch (Exception ex)
            {
                Logger.Error("Failed to send first request, TranscribeInitial, channel: " + chIndex + ", " + ex.ToString());
                result = false;
            }            

            return result;
        }

        static bool TranscribeMiddle(int chIndex, IntPtr iptrMiddle)
        {
            bool result = true;
            bool bSaveEnabledToFile, bPossibleToWriteToSaveFile;

            try
            {                           
                bSaveEnabledToFile = AudioChannelConfig.GetConfig(chIndex).Enabled && AudioChannelConfig.GetConfig(chIndex).SaveWavFile;
                bPossibleToWriteToSaveFile = bSaveEnabledToFile && m_arrAsioWaveFileWriter[chIndex] != null && m_arrAsioWaveFileWriter[chIndex].CanWrite;
                WriteSamplesToByteArray(chIndex, new List<IntPtr> { iptrMiddle }, m_SampleType, m_SamplePerBuffer, bPossibleToWriteToSaveFile);

                m_srStream[chIndex].WriteAsync(
                     new StreamingRecognizeRequest()
                     {
                         AudioContent = Google.Protobuf.ByteString.CopyFrom(m_soundBytesByChNumber[chIndex])
                     }).Wait();
            }
            catch (Exception ex)
            {
                Logger.Error("Failed to send first request,TranscribeInitial, channel: " + chIndex + ", " + ex.ToString());
                result = false;
            }          

            return result;
        }

        //static long sL, eL;
        static bool TranscribeLast(int chIndex, IntPtr iptrLast)
        {
            bool result = true;
            bool bSaveEnabledToFile, bPossibleToWriteToSaveFile;

            try
            {
                //sL = DateTime.Now.Ticks;
                bSaveEnabledToFile = AudioChannelConfig.GetConfig(chIndex).Enabled && AudioChannelConfig.GetConfig(chIndex).SaveWavFile;
                bPossibleToWriteToSaveFile = bSaveEnabledToFile && m_arrAsioWaveFileWriter[chIndex] != null && m_arrAsioWaveFileWriter[chIndex].CanWrite;
                WriteSamplesToByteArray(chIndex, new List<IntPtr> { iptrLast }, m_SampleType, m_SamplePerBuffer, bPossibleToWriteToSaveFile);                

                m_srStream[chIndex].WriteAsync(
                     new StreamingRecognizeRequest()
                     {
                         AudioContent = Google.Protobuf.ByteString.CopyFrom(m_soundBytesByChNumber[chIndex])
                     }).Wait();                

                m_srStream[chIndex].WriteCompleteAsync().Wait();
                ResponseHandlerTask(chIndex, m_srStream[chIndex]).Wait();
                //eL = DateTime.Now.Ticks;

                //if ((eL - sL) > 10000)
                    //Console.WriteLine(chIndex + "=" + (eL - sL) / 10000);
            }
            catch (Exception ex)
            {
                Logger.Error("Failed to send last request,TranscribeLast, channel: " + chIndex + ", " + ex.ToString());
                result = false;
            }

            try
            {
                if (m_arrAsioWaveFileWriter[chIndex] != null)
                {
                    m_arrAsioWaveFileWriter[chIndex].Flush();
                    m_arrAsioWaveFileWriter[chIndex].Close();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Failed to close save file for channel: " + chIndex + ", " + ex.ToString());
            }           

            return result;
        }

        static void WriteSamplesToByteArray(int chIndex, List<IntPtr> lstIptrs, NAudio.Wave.Asio.AsioSampleType ast, int samplesPerBuffer, bool bFileWrite)
        {
            byte[] bytesAudio = new byte[lstIptrs.Count * samplesPerBuffer * 2];//Linear 16 is the format used for google API SERVICE USAGE

            unsafe
            {
                for (int i = 0; i < lstIptrs.Count; i++)
                {
                    if (lstIptrs[i] == IntPtr.Zero)
                    {
                        continue;
                    }

                    for (int j = 0; j < samplesPerBuffer; j++)
                    {
                        switch (ast)
                        {
                            case AsioSampleType.Int16LSB:
                                {
                                    short* lstStart = (short*)lstIptrs[i];
                                    short s16 = *(lstStart + j);
                                    byte lsb = (byte)(s16 & 0xFF);
                                    byte msb = (byte)(s16 >> 8);
                                    bytesAudio[i * samplesPerBuffer * 2 + j * 2] = lsb;
                                    bytesAudio[i * samplesPerBuffer * 2 + j * 2 + 1] = msb;

                                    if(bFileWrite)
                                    {
                                        m_arrAsioWaveFileWriter[chIndex].WriteByte(lsb);
                                        m_arrAsioWaveFileWriter[chIndex].WriteByte(msb);
                                    }
                                    break;
                                }

                            case AsioSampleType.Int32LSB:
                                {
                                    int* lstStart = (int*)lstIptrs[i];
                                    int s32 = *(lstStart + j);
                                    s32 = s32 >> 16; //use upper 16 bits as the most useful ones
                                    byte lsb = (byte)(s32 & 0xFF);
                                    byte msb = (byte)(s32 >> 8);
                                    bytesAudio[i * samplesPerBuffer * 2 + j * 2] = lsb;
                                    bytesAudio[i * samplesPerBuffer * 2 + j * 2 + 1] = msb;

                                    if (bFileWrite)
                                    {
                                        m_arrAsioWaveFileWriter[chIndex].WriteByte(lsb);
                                        m_arrAsioWaveFileWriter[chIndex].WriteByte(msb);
                                    }
                                    break;
                                }
                        }
                    }
                }
            }

            m_soundBytesByChNumber[chIndex] = bytesAudio;

            if(bytesAudio.Length == 0)
            {
                Logger.Warn("Zero length audio data prepared: ", chIndex);
            }
        }

        static async Task<(bool, string)> ResponseHandlerTask(int chIndex, StreamingRecognizeStream srStream)
        {
            string resString = "";
            bool resBool = false;
            //Logger.Debug("ResponseHandlerTask Channel = {0}, Id={1}" , chIndex, Task.CurrentId);

            try
            {
                AsyncResponseStream<StreamingRecognizeResponse> responseStream = srStream.GetResponseStream();

                while (await responseStream.MoveNextAsync())
                {
                    StreamingRecognizeResponse responseItem = responseStream.Current;

                    resString = responseItem.Results.Last().Alternatives.Last().Transcript.ToString();

                    if (responseItem.Results[responseItem.Results.Count - 1].IsFinal)
                    {
                        Logger.Debug("--->Final transcript for channel {0} = \"{1}\"", chIndex, resString);
                        resBool = true;

                        if (m_IsDBAvailable)
                        {
                            EnqueueForDBSave(chIndex, resString);
                        }
                        else
                        {
                            Logger.Warn("Database not available for saving. Skipping save.");
                        }
                        break;
                    }
                    else
                    {
                        Logger.Debug("->Interim transcript for channel {0} = {1}", chIndex, resString);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error in ResponseHandlerTask: " + ex.ToString());
            }

            return (resBool, resString);
        }

        static int SeqNum = 0;
        static void EnqueueForDBSave(int chIndex, string strTranscript)
        {            
            AudioChannelConfig chCfg = AudioChannelConfig.GetConfig(chIndex);
            if (chCfg != null)
            {
                try
                {
                    TextMessage tm = new TextMessage();
                    tm.ExpiryTime = DateTime.UtcNow.AddSeconds(chCfg.Expiry_Seconds);
                    tm.GroupName = chCfg.GroupName;

                    if (strTranscript.Length > TextMessage.MAX_LEN_MSG)
                    {
                        Logger.Warn("Trimming length of message before saving to: " + TextMessage.MAX_LEN_MSG);
                        strTranscript = strTranscript.Substring(0, TextMessage.MAX_LEN_MSG);
                    }

                    tm.Message = strTranscript;
                    tm.Priority = chCfg.Priority;
                    tm.SeqNumber = SeqNum++;
                    tm.TimeToDisplay = DateTime.UtcNow;
                    m_QueueRTT.Enqueue(tm);
                }
                catch (Exception ex)
                {
                    Logger.Error("Failed to enqueue message for saving in DB, channel: " + chIndex + ", " + ex.ToString());
                }                
            }
        }      

        static bool IsIdle(IntPtr ipSamples, int countSamples, NAudio.Wave.Asio.AsioSampleType ast)
        {
            bool result = true;

            unsafe
            {
                switch (ast)
                {
                    case NAudio.Wave.Asio.AsioSampleType.Int32LSB:
                        for (int i = 0; i < countSamples; i++)
                        {
                            if ( *((int*)ipSamples + i) > SettingsHelper.ThresInt32Max || *((int*)ipSamples + i) < SettingsHelper.ThresInt32Min)
                            {
                                result = false;
                                break;
                            }
                        }
                        break;

                    case NAudio.Wave.Asio.AsioSampleType.Int16LSB:
                        for (int i = 0; i < countSamples; i++)
                        {
                            if ( *((short*)ipSamples + i) > SettingsHelper.ThresInt16Max || *((short*)ipSamples + i) < SettingsHelper.ThresInt16Min)
                            {
                                result = false;
                                break;
                            }
                        }
                        break;


                    case NAudio.Wave.Asio.AsioSampleType.Int24LSB:
                        for (int i = 0; i < countSamples; i++)
                        {
                            byte* pSample = (byte*)(ipSamples + i*3);
                            int sample = pSample[0] | (pSample[1] << 8) | ((sbyte)pSample[2] << 16);

                            if (sample > SettingsHelper.ThresInt24Max || sample < SettingsHelper.ThresInt24Min)
                            {
                                result = false;
                                break;
                            }
                        }
                        break;

                    case NAudio.Wave.Asio.AsioSampleType.Float32LSB:
                        for (int i = 0; i < countSamples; i++)
                        {
                            if ( *((float*)ipSamples + i) > SettingsHelper.ThresFloat ||  *((float*)ipSamples + i) < -SettingsHelper.ThresFloat)
                            {
                                result = false;
                                break;
                            }
                        }

                        break;

                    default:
                        throw new Exception("Unimplemented data type in NAUDIO: " + ast.ToString());                        
                }
            }

            return result;
        }    
        
        static void AddToPrependBuffer(int chIndex, IntPtr iptr)
        {
            int sizeAlloc = 0;

            /////////////////////// Make copy in bounded buffer (for prepended buffer)
            if (m_prePendSamplesByChNumber[chIndex] != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(m_prePendSamplesByChNumber[chIndex]);
                m_prePendSamplesByChNumber[chIndex] = IntPtr.Zero;
            }

            unsafe
            {
                m_prePendSamplesByChNumber[chIndex] = SettingsHelper.AllocateMemory(m_SamplePerBuffer, m_SampleType, out sizeAlloc);
                Buffer.MemoryCopy((void*)iptr, (void*)m_prePendSamplesByChNumber[chIndex], sizeAlloc, sizeAlloc);
            }
            /////////////////////////////
        }
    }
}
