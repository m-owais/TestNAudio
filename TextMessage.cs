﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TestNAudio
{
    public partial class TextMessage
    {
        public const int MAX_LEN_MSG = 512;
        public long Id { get; set; }
        public long SeqNumber { get; set; }
        public string GroupName { get; set; }
        public string Message { get; set; }
        public int Priority { get; set; }
        public DateTime TimeToDisplay { get; set; }
        public DateTime ExpiryTime { get; set; }
    }
}
