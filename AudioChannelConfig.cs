﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace TestNAudio
{
    /// <summary>
    /// Corresponds to the JSON object in settings.
    ///        "ChannelIndex": 0,
    ///        "GroupName": "Boardroom",
    ///        "Priority": 10,
    ///        "Expiry_Seconds": 60
    /// </summary>
    public class AudioChannelConfig
    {
        public const string SETTINGS_FILE_NAME = "AudioChannelSettings.json";
        static AudioChannelConfig[] m_Configs;

        public int ChannelIndex { get; set; }
        public bool Enabled { get; set; }
        public bool SaveWavFile { get; set; }
        public string GroupName { get; set; }
        public int Priority { get; set; }
        public int Expiry_Seconds { get; set; }
        public string SavePath { get; set; }

        public AudioChannelConfig() { }

        static string GetFileContents()
        {
            string result = File.ReadAllText(SETTINGS_FILE_NAME);
            return result;
        }

        public static bool ReadSettingsFromJson()
        {
            bool result = false;

            try
            {
                string strJson = GetFileContents();
                m_Configs = JsonSerializer.Deserialize<AudioChannelConfig[]>(strJson);
                result = true;
            }
            catch (Exception ex)
            {
                Logger.Error("Error loading settings from Json file: " + ex.ToString());
            }

            return result;
        }
        public static AudioChannelConfig GetConfig(int index)
        {
            AudioChannelConfig result = null;

            //match index to ChannelIndex
            for (int i = 0; i < m_Configs.Length; i++)
                if (m_Configs[i] != null && m_Configs[i].ChannelIndex == index)
                {
                    result = m_Configs[i];
                    break;
                }            
            return result;
        }

        public string GetSavePath()
        {
            string dir;

            if (string.IsNullOrEmpty(SavePath))
                dir = System.IO.Directory.GetCurrentDirectory();
            else
                dir = SavePath;

            string result = dir + "\\LogAudio-" + ChannelIndex + "-" + DateTime.Now.ToString("yyyyMMdd-hhmmss") + ".wav";
            return result;
        }
    }
}
