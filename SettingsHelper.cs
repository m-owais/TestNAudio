﻿using Google.Cloud.Speech.V1;
using NAudio.Wave.Asio;
using System;
using System.Configuration;
using System.Runtime.InteropServices;

namespace TestNAudio
{
    public class SettingsHelper
    {
        const float DEFAULT_THRESHOLD_DETECT_AUDIO = 0.05f;
        const int DEFAULT_IDLE_SECS = 5;
        const int DEFAULT_SAMPLE_RATE = 44100;
        const bool DEFAULT_PROFANITY_FILTER_ENABLED = true;
        const int DEFAULT_DB_MSG_DELETE_HOURS = 24;

        public static readonly Int16 ThresInt16Max;
        public static readonly Int16 ThresInt16Min;
        public static readonly Int32 ThresInt24Max;
        public static readonly Int32 ThresInt24Min;
        public static readonly Int32 ThresInt32Max;
        public static readonly Int32 ThresInt32Min;
        public static readonly float ThresFloat;
        public static readonly int SampleRate;
        public static readonly bool ProfanityFilter;
        public static readonly string EnglishAccentSettings;

        static SettingsHelper()
        {
            ThresInt16Max = (Int16)(Int16.MaxValue * GetThresholdAudioDetect());
            ThresInt16Min = (Int16)(Int16.MinValue * GetThresholdAudioDetect());
            ThresInt24Max = (Int32)(8388607 * GetThresholdAudioDetect()); //2^23-1
            ThresInt24Min = (Int32)(-8388608 * GetThresholdAudioDetect()); //-2^23
            ThresInt32Max = (Int32)(Int32.MaxValue * GetThresholdAudioDetect());
            ThresInt32Min = (Int32)(Int32.MinValue * GetThresholdAudioDetect());
            ThresFloat = GetThresholdAudioDetect();
            SampleRate = GetSampleRate();
            ProfanityFilter = GetProfanityFilterEnabled();
            EnglishAccentSettings = GetEnglishAccentSetting();
        }

        public static float GetThresholdAudioDetect()
        {
            float result = DEFAULT_THRESHOLD_DETECT_AUDIO;
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Threshold_Audio_Detect"]) == false)            
                float.TryParse(ConfigurationManager.AppSettings["Threshold_Audio_Detect"], out result);            

            return result;
        }

        public static int GetIdleTimeSeconds()
        {
            int result = DEFAULT_IDLE_SECS;
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Idle_Duration_Seconds"]) == false)
                int.TryParse(ConfigurationManager.AppSettings["Idle_Duration_Seconds"], out result);

            return result;
        }

        public static int GetSampleRate()
        {
            int result = DEFAULT_SAMPLE_RATE;
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Sample_Rate"]) == false)
                int.TryParse(ConfigurationManager.AppSettings["Sample_Rate"], out result);

            return result;
        }

        public static bool GetProfanityFilterEnabled()
        {
            bool result = DEFAULT_PROFANITY_FILTER_ENABLED;
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Profanity_Filter_Enable"]) == false)
                bool.TryParse(ConfigurationManager.AppSettings["Profanity_Filter_Enable"], out result);

            return result;
        }

        public static int GetMaxIdleFrameCount(int samplesPerBuffer)
        {
            int result = (SampleRate  * GetIdleTimeSeconds()) / samplesPerBuffer;
            return result;
        }        

        public static string GetEnglishAccentSetting()
        {
            string result = "";

            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["English_Accent"]) == false)
                result = ConfigurationManager.AppSettings["English_Accent"];

            switch(result.ToUpper())
            {
                case "AUSTRALIA":
                    result = LanguageCodes.English.Australia;
                    break;
                case "CANADA":
                    result = LanguageCodes.English.Canada;                    
                    break;
                case "GHANA":
                    result = LanguageCodes.English.Ghana;
                    break;
                case "INDIA":
                    result = LanguageCodes.English.India;
                    break;
                case "IRELAND":
                    result = LanguageCodes.English.Ireland;
                    break;
                case "KENYA":
                    result = LanguageCodes.English.Kenya;
                    break;
                case "NEWZEALAND":
                    result = LanguageCodes.English.NewZealand;
                    break;
                case "NIGERIA":
                    result = LanguageCodes.English.Nigeria;
                    break;
                case "PHILIPPINES":
                    result = LanguageCodes.English.Philippines;
                    break;
                case "SINGAPORE":
                    result = LanguageCodes.English.Singapore;
                    break;
                case "SOUTHAFRICA":
                    result = LanguageCodes.English.SouthAfrica;
                    break;
                case "TANZANIA":
                    result = LanguageCodes.English.Tanzania;
                    break;
                case "UNITEDKINGDOM":
                    result = LanguageCodes.English.UnitedKingdom;
                    break;
                case "UNITEDSTATES":
                    result = LanguageCodes.English.UnitedStates;
                    break;
                default:
                    Logger.Error("Unknown value for english accent. Using Australia as default");
                    result = LanguageCodes.English.Australia;
                    break;
            }
            Logger.Info("Chosen english accent: " + result);
            return result;
        }

        public static IntPtr AllocateMemory(int sampleCount, AsioSampleType ast, out int sizeOutBufferBytes)
        {
            IntPtr result = IntPtr.Zero;
            sizeOutBufferBytes = 0;

            unsafe
            {
                switch (ast)
                {
                    case NAudio.Wave.Asio.AsioSampleType.Int32LSB:
                        sizeOutBufferBytes = sizeof(Int32) * sampleCount;                        
                        break;

                    case NAudio.Wave.Asio.AsioSampleType.Int24LSB:
                        sizeOutBufferBytes = 3 * sampleCount;
                        break;

                    case NAudio.Wave.Asio.AsioSampleType.Int16LSB:
                        sizeOutBufferBytes = sizeof(Int16) * sampleCount;
                        break;                    

                    case NAudio.Wave.Asio.AsioSampleType.Float32LSB:
                        sizeOutBufferBytes = sizeof(float) * sampleCount;
                        break;

                    default:
                        throw new Exception("Unimplemented data type in NAUDIO: " + ast.ToString());
                }
            }

            if(sizeOutBufferBytes != 0)
                result = Marshal.AllocHGlobal(sizeOutBufferBytes);

            return result;
        }

        public static RecognitionConfig GetRecognitionCfg()
        {
            var config = new RecognitionConfig
            {
                AudioChannelCount = 1, //fixed for now
                Encoding = RecognitionConfig.Types.AudioEncoding.Linear16, //fixed for now
                SampleRateHertz = SettingsHelper.SampleRate, //from CFG file                
                ProfanityFilter = SettingsHelper.ProfanityFilter, //from CFG file
                LanguageCode = SettingsHelper.EnglishAccentSettings //from CFG file  
            };

            return config;
        }

        public static int GetAsioDriverIndex(string[] strDrivers)
        {
            int result = -1;
            string strDriverName = "";

            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Asio_Driver_Name"]) == false)
                strDriverName = ConfigurationManager.AppSettings["Asio_Driver_Name"];

            if (strDrivers == null || strDrivers.Length == 0)
            {
                Logger.Error("ASIO driver is not installed. Aborting");
            }
            else
            {
                if (string.IsNullOrEmpty(strDriverName))
                {
                    Logger.Warn("ASIO driver is not specified in config file. Using default.");
                    result = 0;
                }
                else
                {
                    for(int i=0; i < strDrivers.Length; i++)
                    {   
                        if(strDrivers[i] == strDriverName)
                        {
                            Logger.Info("Using driver index {0} for application", i);
                            result = i;
                            break;
                        }
                    }
                }
            }

            if(result == -1)
            {
                Logger.Error("Provided driver name {0} did not match any installed driver name. Aborting", strDriverName);
            }

            return result;
        }

        public static int GetDbHoursDelete()
        {
            int result = DEFAULT_DB_MSG_DELETE_HOURS;
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["DB_Delete_By_Old_Hours"]) == false)
                int.TryParse(ConfigurationManager.AppSettings["DB_Delete_By_Old_Hours"], out result);

            return result;
        }

        public static int GetMaximumTranscribeSeconds()
        {
            int result = 300;
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Max_Transcribe_Length_Secs"]) == false)
                int.TryParse(ConfigurationManager.AppSettings["Max_Transcribe_Length_Secs"], out result);

            if(result > 300)
            {
                result = 300;
                Console.WriteLine("*** Truncating Max_Transcribe_Length_Secs to 300 seconds as value too large ***");
            }

            return result;
        }

        /// <summary>
        /// The google service allows maximum 300 seconds of audio to be transcribed in a single streaming session.
        /// If the audio exceeds that we break the session and start a new one on recepit of a new audio frame
        /// 
        /// we count the number of frames received instead of timing the 300 seconds interval 
        /// 
        /// </summary>
        /// <param name="samplesPerBuffer"></param>
        /// <returns></returns>
        public static int GetMaxAllowedFrameCount(int samplesPerBuffer)
        {
            int result;
            result = (SettingsHelper.GetMaximumTranscribeSeconds() * SettingsHelper.SampleRate) / samplesPerBuffer;
            return result;
        }
    }
}
