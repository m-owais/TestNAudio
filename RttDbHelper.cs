﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace TestNAudio
{
    public class RttDbHelper
    {
        Realtime_TextContext m_RttDbContext = null;

        public bool Open()
        {
            bool result = false;
            try
            {
                m_RttDbContext = new Realtime_TextContext();                
                result = true;
            }
            catch (Exception ex)
            {
                Logger.Error("Error opening database RTT connection: " + ex.ToString());
            }      
            return result;
        }
        public bool IsDBAvailable()
        {
            bool result = false;
            try
            {
                result = m_RttDbContext.Database.CanConnect();
            }
            catch (Exception ex)
            {
                Logger.Error("Error checking for Realtime_TextContext connection: " + ex.ToString());
            }           
            return result;
        }

        public Realtime_TextContext GetContext()
        {
            return m_RttDbContext;
        }

        public void DeleteMessagesOlderThanHours(int hours)
        {
            DateTime dtThres = DateTime.Now.AddHours(-hours);
            TextMessage[] messages = m_RttDbContext.TextMessages.Where(tm => tm.TimeToDisplay <= dtThres).ToArray();

            if (messages != null && messages.Length > 0)
            {
                Logger.Info("Found messages to purge from database, count = {0}", messages.Length);
                m_RttDbContext.TextMessages.RemoveRange(messages);
                m_RttDbContext.SaveChanges();
            }
            else
            {
                Logger.Info("Found NO messages to purge from database");
            }
        }
    }
}
