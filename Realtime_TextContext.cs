﻿using System;
using System.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace TestNAudio
{
    public partial class Realtime_TextContext : DbContext
    {
        public Realtime_TextContext()
        {
        }

        public Realtime_TextContext(DbContextOptions<Realtime_TextContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TextMessage> TextMessages { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql(ConfigurationManager.ConnectionStrings["CONN_STR_RTT_DB"].ConnectionString, Microsoft.EntityFrameworkCore.ServerVersion.Parse("10.5.10-mariadb"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasCharSet("utf8mb4")
                .UseCollation("utf8mb4_general_ci");

            modelBuilder.Entity<TextMessage>(entity =>
            {
                entity.ToTable("text_message");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("id");

                entity.Property(e => e.ExpiryTime)
                    .HasColumnType("datetime(3)")
                    .HasColumnName("expiry_time");

                entity.Property(e => e.GroupName)
                    .IsRequired()
                    .HasMaxLength(190)
                    .HasColumnName("group_name");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasMaxLength(512)
                    .HasColumnName("message");

                entity.Property(e => e.Priority)
                    .HasColumnType("int(11)")
                    .HasColumnName("priority");

                entity.Property(e => e.SeqNumber)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("seq_number");

                entity.Property(e => e.TimeToDisplay)
                    .HasColumnType("datetime(3)")
                    .HasColumnName("time_to_display");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
